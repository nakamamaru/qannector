import logging
import math
from concurrent.futures import ThreadPoolExecutor
from typing import List, Tuple, Optional, Callable

import multiprocess.pool
import numpy as np

from qannector import tensorflow_layer as tfl
from qannector.functions import divide_collection_into_batches
from qannector.image_manipulation import resize_image, cut_image_into_chunks, pad_images
from qannector.kmp_duplicate_lib_decorator import save_and_restore_kmp_duplicate_lib_ok
from qannector.metadata import get_model_input_size_from_metadata
from qannector.metrics import log_metrics, ConnectorMetrics
from qannector.model_urls import get_model_predict_url
from qannector.parallelism_mode import ParallelismMode
from qannector.resize_options import ResizeOptions


def __validate_and_set_batch_size(base_batch_size: int,
                                  thread_count: int,
                                  image_count: int,
                                  comfort_max_batch_size: Optional[int],
                                  logger: logging.Logger) -> int:
    if comfort_max_batch_size is None or comfort_max_batch_size <= 0:
        return base_batch_size

    if comfort_max_batch_size * thread_count >= image_count:
        batch_size = math.ceil(image_count / thread_count)
        logger.debug(f'Batch size set {batch_size} due to comforting algorythm. '
                     f'image_count: {image_count}, thread_count: {thread_count}, '
                     f'comfort_max_batch_size: {comfort_max_batch_size}')
        return batch_size

    return base_batch_size


@save_and_restore_kmp_duplicate_lib_ok
@log_metrics('qannector.metrics')
def apply_segmentation_model(images: List[np.ndarray],
                             tfs_url: str,
                             model_name: str,
                             model_version: int = 1,
                             batch_size: int = 90,
                             comfort_max_batch_size: Optional[int] = None,
                             resize_options: Optional[ResizeOptions] = None,
                             normalization: Optional[Callable[[np.ndarray], np.ndarray]] = None,
                             thread_count: int = 4,
                             parallelism_mode: int = -1,
                             request_timeout_s: int = 300,
                             max_retry_count: int = 1,
                             strict_mode_logging: bool = False,
                             channels: Optional[List[int]] = None) -> Tuple[
    List[np.ndarray], ConnectorMetrics]:
    """
    Applies remote ANN segmentation model (served by TensorFlow Serving) to a list of images in a series of API
    requests, one request for each batch. The list of images is broken into multiple parts, each part processed in a
    separate thread. In each thread each image is cut into chunks according to chunk_size parameter, then each chunk
    will be resized to model_input_size. All chunks of a batch will be smushed together and sent to TFS to process.

    :param strict_mode_logging: If set to False all non-critical warnings will be presented
    in logs in INFO levels instead of WARNING level
    :param parallelism_mode: parallelism mode. Use ParallelismMode.PARALLELISM_MODE_* constants to fill correctly.
    Default multiprocess.
    :param resize_options: resize options. If omitted, will be replaced with ResizeOptions.default()
    :param images: List of images (np.ndarray) to process
    :param model_name: ANN model name on TFS
    :param model_version: ANN model version
    :param tfs_url: TFS rest api endpoint URL
    :param batch_size: number of images smushed in a single query to ANN
    :param thread_count: number of parallel threads to run. If 0 or 1 passed single-threaded version of a function will
     be run.
    :param normalization: Normalization function to apply when image is converted into tensor
    :param max_retry_count: Max amount of retries to request TFS (in given timeout).
    :param request_timeout_s: Timeout in seconds for TFS prediction (in seconds). Default 300 seconds (5 min).
    :return: Results of ANN processing, list of images (np.ndarray)
    """

    if type(images) != list:
        raise ValueError(
            f'Parameter images expected to be a list of images (np.ndarray): List[np.ndarray], '
            f'received {type(images)} instead')

    if images is None or len(images) == 0:
        return [], ConnectorMetrics.empty(stage_name='TFSSegmentation')

    _metrics = ConnectorMetrics.start(stage_name='TFSSegmentation', images_total=len(images))

    model_predict_url = get_model_predict_url(tfs_url, model_name, model_version)

    main_logger = logging.getLogger('qannector.main')
    flow_logger = logging.getLogger('qannector.flow')

    validation_stage_metrics = ConnectorMetrics.start('Validation', images_total=len(images))

    parallelism_mode = ParallelismMode.validate_and_set_parallelism_mode(parallelism_mode, main_logger)

    if resize_options is None:
        resize_options = ResizeOptions.default()

    chunk_size_px = __get_chunk_size(resize_options, images)
    chunk_size_px = __validate_and_set_chunk_size(chunk_size_px, flow_logger, strict_mode_logging)

    __validate_normalization(flow_logger, normalization, strict_mode_logging)

    model_input_size = __validate_and_set_model_input_size(resize_options.model_input_size, chunk_size_px,
                                                           tfs_url, model_name, model_version,
                                                           flow_logger, strict_mode_logging)

    thread_count = __validate_and_set_thread_count(thread_count, main_logger, strict_mode_logging)

    batch_size = __validate_and_set_batch_size(batch_size, thread_count, len(images),
                                               comfort_max_batch_size, main_logger)

    _metrics.register_stage(validation_stage_metrics)

    padding_stage_metrics = ConnectorMetrics.start(stage_name='PaddingImages', images_total=len(images))

    images = pad_images(images, chunk_size_px)

    _metrics.register_stage(padding_stage_metrics)

    images_split = divide_collection_into_batches(images, batch_size)
    main_logger.debug(f'Batch size: {batch_size}, '
                      f'total images: {len(images)},'
                      f' total batches count: {len(images_split)}, '
                      f'batches: {[len(b) for b in images_split]}')

    # TODO add layers filtering
    result = []
    if parallelism_mode == ParallelismMode.PARALLELISM_MODE_THREADPOOL:
        with ThreadPoolExecutor(max_workers=thread_count) as executor:
            futures = [executor.submit(__apply_segmentation_model_to_batch,
                                       images_batch,
                                       batch_number,
                                       model_predict_url,
                                       chunk_size_px,
                                       model_input_size,
                                       resize_options.chunk2net_resize_interpolation,
                                       resize_options.net2map_resize_interpolation,
                                       normalization,
                                       request_timeout_s,
                                       max_retry_count,
                                       channels)
                       for (batch_number, images_batch) in enumerate(images_split)]

            for future in futures:
                batch_results, stages_metrics = future.result()
                result.extend(batch_results)
                _metrics.register_stage(stages_metrics)

    elif parallelism_mode == ParallelismMode.PARALLELISM_MODE_MULTIPROCESS:
        with multiprocess.pool.Pool(thread_count) as pool:
            mpp_results = [pool.apply_async(__apply_segmentation_model_to_batch,
                                            (
                                                images_batch,
                                                batch_number,
                                                model_predict_url,
                                                chunk_size_px,
                                                model_input_size,
                                                resize_options.chunk2net_resize_interpolation,
                                                resize_options.net2map_resize_interpolation,
                                                normalization,
                                                request_timeout_s,
                                                max_retry_count,
                                                channels)
                                            )
                           for (batch_number, images_batch) in enumerate(images_split)]

            for mpp_result in mpp_results:
                batch_results, stages_metrics = mpp_result.get()
                result.extend(batch_results)
                _metrics.register_stage(stages_metrics)

    _metrics.stop()
    main_logger.debug(_metrics)

    return result, _metrics


def __validate_and_set_thread_count(thread_count, main_logger, strict_mode_logging):
    if thread_count == 0:
        main_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                        msg='Parameter thread_count passed == 0. Did you mean 1? Running running as if thread_count was 1.')
        thread_count = 1
    return thread_count


def __validate_and_set_model_input_size(model_input_size, chunk_size_px, endpoint_url, model_name, model_version,
                                        flow_logger, strict_mode_logging):
    if model_input_size is None:
        model_input_size = get_model_input_size_from_metadata(endpoint_url, model_name, model_version)
        flow_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                        msg=f'Model Input Size parameter received: None. Model Input Size was received from model '
                            f'metadata: {model_input_size}.')

        if not model_input_size == (-1, -1) and not model_input_size == chunk_size_px:
            flow_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                            msg=f'Model Input Size {model_input_size} is not equal to Chunk Size {chunk_size_px}. Each '
                                f'chunk will be resized to Model Input Size.')

        if model_input_size == (-1, -1):
            flow_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                            msg=f'Model Input Size {model_input_size} means that chunks will be resized by ANN.')

    else:
        if model_input_size == chunk_size_px:
            flow_logger.info(f'Model Input Size parameter received equals to Chunk Size: {model_input_size}. '
                             f'No chunk resizing applied.')
        else:
            flow_logger.info(
                f'Model Input Size received: {model_input_size}. Chunks will be resized from {chunk_size_px} '
                f'to {model_input_size} before sending to ANN.')

    return model_input_size


def __validate_normalization(flow_logger, normalization, strict_mode_logging):
    if normalization is None:
        flow_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                        msg='Normalization function parameter received: None. No additional normalization will be '
                            'applied. Make sure you pass normalized data. You may receive wrong results, '
                            'border artifacts and other imperfections if normalization wasn\'t applied correctly')


def __validate_and_set_chunk_size(chunk_size_px, flow_logger, strict_mode_logging):
    if chunk_size_px is None:
        chunk_size_px = (256, 256)
        flow_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                        msg='Chunk size parameter received: None. Default size was set = (256, 256)')
    return chunk_size_px


def __get_chunk_size(resize_options, images):
    if resize_options.chunking_mode == ResizeOptions.CHUNKING_MODE_STATIC:
        chunk_size = resize_options.chunk_size
    elif resize_options.chunking_mode == ResizeOptions.CHUNKING_MODE_DYNAMIC:
        chunk_size = resize_options.get_dynamic_chunk_size((images[0].shape[0], images[0].shape[1]))
    else:
        raise ValueError(f'''Chunking mode {resize_options.chunking_mode} not supported by segmentation''')
    return chunk_size


@log_metrics('qannector.metrics')
def __apply_segmentation_model_to_batch(all_images: List[np.ndarray],
                                        batch_number: int,
                                        model_predict_url: str,
                                        chunk_size: Tuple[int, int],
                                        model_input_size: Tuple[int, int],
                                        chunk2net_resize_interpolation: int,
                                        net2map_resize_interpolation: int,
                                        normalization: Optional[Callable[[np.ndarray], np.ndarray]] = None,
                                        request_timeout_s: int = 300,
                                        max_retry_count: int = 1,
                                        channels: Optional[List[int]] = None) \
        -> Tuple[List[np.ndarray], ConnectorMetrics]:
    """
    Applies remote TensorFlow Serving model to a batch of images

    :param all_images: Full list of images (to avoid extra copying)
    :param model_predict_url: TensorFlow Serving REST API Predict method of a chosen model
    :param chunk_size: Size of a chunk each image in batch will be cut into
    :param model_input_size: Model input size each chunk will be resized to
    :param normalization: Normalization function to apply when image is converted into tensor
    :param max_retry_count: Max amount of retries to request TFS (in given timeout).
    :param request_timeout_s: Timeout in seconds for TFS prediction (in seconds). Default 300 seconds (5 min).
    :return: List of resulting images
    """
    batch_thickness = len(all_images)
    original_image_size = (all_images[0].shape[0], all_images[0].shape[1])

    _metrics = ConnectorMetrics.start(f"ApplySegmentationModelToBatch#{batch_number}", images_total=batch_thickness)

    _streamlining_stage_metrics = ConnectorMetrics.start("TFSInputBatchCreation", images_total=batch_thickness)
    tfs_input_batch = __create_tfs_input_for_batch(all_images,
                                                   chunk_size,
                                                   model_input_size,
                                                   chunk2net_resize_interpolation=chunk2net_resize_interpolation,
                                                   normalization=normalization)

    _metrics.register_stage(_streamlining_stage_metrics)

    predictions, _tf_prediction_metrics = tfl.get_tensorflow_prediction(tfs_input_batch, model_predict_url,
                                                                        request_timeout_s=request_timeout_s,
                                                                        max_retry_count=max_retry_count)

    _metrics.register_stage(_tf_prediction_metrics)

    _mask_calc_stage_metrics = ConnectorMetrics.start("MaskPatchCalculation", images_total=batch_thickness)
    linear_mask = __calculate_mask_patches_for_tfs_predictions(predictions, chunk_size,
                                                               net2map_resize_interpolation=net2map_resize_interpolation)
    _metrics.register_stage(_mask_calc_stage_metrics)

    if channels is not None and len(channels) > 0:
        channels_extracting_stage = ConnectorMetrics.start("ChannelExtraction", images_total=len(linear_mask))
        linear_mask = [item[..., channels] for item in linear_mask]
        _metrics.register_stage(channels_extracting_stage)

    _pathology_maps_stage_metrics = ConnectorMetrics.start("PathologyMapsCalculation", images_total=len(linear_mask))
    pathology_maps = __combine_masks_into_pathology_maps(linear_mask,
                                                         batch_thickness,
                                                         original_image_size,
                                                         chunk_size)

    _metrics.register_stage(_pathology_maps_stage_metrics)

    _metrics.stop()
    return pathology_maps, _metrics


def __calculate_mask_patches_for_tfs_predictions(predictions: list, chunk_size: Tuple[int, int],
                                                 net2map_resize_interpolation: int) -> List[np.ndarray]:
    """
    Calculates masks from TensorFlow predictions, resizes them and returns as a linear list of ndarrays
    :param predictions: A list of TensorFlow predictions
    :param chunk_size: Size of a chunk mask will be resized to
    :return: Linear list of masks made from TensorFlow predictions
    """
    linear_tfserving_response = []
    for prediction in predictions:
        mask_patch = np.array(prediction)
        resized_mask_patch = resize_image(mask_patch, chunk_size, interpolation=net2map_resize_interpolation)
        linear_tfserving_response.append(resized_mask_patch)
    return linear_tfserving_response


@log_metrics('qannector.metrics')
def __create_tfs_input_for_batch(images: List[np.ndarray],
                                 chunk_size: Tuple[int, int],
                                 model_input_size: Tuple[int, int],
                                 chunk2net_resize_interpolation: int,
                                 normalization: Optional[Callable[[np.ndarray], np.ndarray]] = None) \
        -> List[np.ndarray]:
    """
    Creates an input (list of tensors) for a TensorFlow from a chunk of list of (padded) images
    :param images: Full list of images (to avoid extra copying)
    :param chunk_size: Size of a chunk each image in batch will be cut into
    :param model_input_size: Model input size each chunk will be resized to
    :param normalization: Normalization function to apply when image is converted into tensor
    :return: A list of tensors size of model_input_size ready to be served into TensorFlow
    """
    result = []
    for idx in range(len(images)):
        chunked_image: List[np.ndarray] = cut_image_into_chunks(images[idx],
                                                                chunk_size,
                                                                model_input_size,
                                                                interpolation=chunk2net_resize_interpolation,
                                                                normalization=normalization)
        result.extend(chunked_image)
    return result


def __create_mask_default(prediction: list) -> np.ndarray:
    """
    Calculates a mask of multichannel prediction using argmax function
    :param prediction: TensorFlow prediction
    :return: single channel mask image
    """
    prediction = np.argmax(prediction, -1)
    prediction = prediction.astype(np.uint16)
    prediction = prediction[..., np.newaxis]
    squeezed = np.squeeze(prediction, axis=2)
    return squeezed


@log_metrics('qannector.metrics')
def __combine_masks_into_pathology_maps(linear_mask: List[np.ndarray],
                                        batch_thickness: int,
                                        original_image_size: Tuple[int, int],
                                        chunk_size: Tuple[int, int]) -> List[np.ndarray]:
    """
    Combines linear mask of TensorFlow response into a list of images
    :param linear_mask: Linear mask of TensorFlow response
    :param batch_thickness: A number of images in mask
    :param original_image_size: Size of original image
    :param chunk_size: Size of a chunk request was cut into
    :return: A list of single-channel images (pathology maps)
    """

    # TODO it seems that there's too many parameters, may be we can calculate some of them in-place

    classes_count = linear_mask[0].shape[2]
    chunk_shape = (chunk_size[0], chunk_size[1], classes_count)
    pathology_map_shape = (original_image_size[0], original_image_size[1], classes_count)

    request_map_shape = (original_image_size[0] // chunk_size[0], original_image_size[1] // chunk_size[1])
    result = []
    for idx in range(0, batch_thickness):

        pathology_map = np.zeros(pathology_map_shape)
        patch_start = idx * request_map_shape[0] * request_map_shape[1]
        patch_end = (idx + 1) * request_map_shape[0] * request_map_shape[1]

        patchwork = np.reshape(linear_mask[patch_start:patch_end],
                               (request_map_shape[0], request_map_shape[1], classes_count, -1))

        for x in range(request_map_shape[0]):
            for y in range(request_map_shape[1]):
                pathology_map[
                (x * chunk_size[0]): (x + 1) * chunk_size[0],
                y * chunk_size[1]: (y + 1) * chunk_size[1]
                ] = np.reshape(patchwork[x][y], chunk_shape)
        result.append(pathology_map)

    return result
