import platform


class ParallelismMode:
    PARALLELISM_MODE_AUTO = -1
    PARALLELISM_MODE_MULTIPROCESS = 0
    PARALLELISM_MODE_THREADPOOL = 1

    ALLOWED_PARALLELISM_MODES = [
        PARALLELISM_MODE_AUTO,
        PARALLELISM_MODE_MULTIPROCESS,
        PARALLELISM_MODE_THREADPOOL
    ]

    @staticmethod
    def validate_and_set_parallelism_mode(parallelism_mode, main_logger):
        if parallelism_mode not in ParallelismMode.ALLOWED_PARALLELISM_MODES:
            raise ValueError(f'Parameter parallelism_mode={parallelism_mode} has value that is not '
                             f'in allowed parallelism modes={ParallelismMode.ALLOWED_PARALLELISM_MODES}')
        if parallelism_mode == ParallelismMode.PARALLELISM_MODE_AUTO:
            parallelism_mode = ParallelismMode.PARALLELISM_MODE_THREADPOOL if platform.system() == "Windows" \
                else ParallelismMode.PARALLELISM_MODE_MULTIPROCESS
            main_logger.debug(
                f'Multiprocess parallelism mode received: ParallelismMode.PARALLELISM_MODE_AUTO. Set {parallelism_mode} as platform.system() == {platform.system()}')
            return parallelism_mode

        if platform.system() == "Windows" and parallelism_mode == ParallelismMode.PARALLELISM_MODE_MULTIPROCESS:
            main_logger.warning('Multiprocess parallelism mode not well-suited for Windows system. '
                                'Try use ParallelismMode.PARALLELISM_MODE_THREADPOOL.')

        elif platform.system() == "Linux" and parallelism_mode == ParallelismMode.PARALLELISM_MODE_THREADPOOL:
            main_logger.warning('Multiprocess parallelism mode not well-suited for Windows system. '
                                'Try use ParallelismMode.PARALLELISM_MODE_MULTIPROCESS.')

        return parallelism_mode
