import datetime
import json
import uuid
from dataclasses import dataclass, field
from typing import List, Union, Any, Dict


@dataclass
class ConnectorMetrics:
    stage_name: Union[str, None] = None
    __time_start: Union[datetime.datetime, None] = None
    images_total: Union[int, None] = None
    time_total_s: Union[float, None] = None
    stages: List["ConnectorMetrics"] = field(default_factory=list)
    extra: Union[Dict[Any, Any], None] = None

    @staticmethod
    def start(stage_name: Union[str, None], images_total: Union[int, None] = None) -> "ConnectorMetrics":
        metrics = ConnectorMetrics(stage_name=stage_name)
        metrics.images_total = images_total
        metrics.__time_start = datetime.datetime.utcnow()
        return metrics

    def stop(self) -> "ConnectorMetrics":
        self.time_total_s = (datetime.datetime.utcnow() - self.__time_start).total_seconds()
        return self

    def register_stage(self, stage: "ConnectorMetrics", stop_stage=True):
        if stop_stage and stage.time_total_s is None:
            stage.stop()
        self.stages.append(stage)

    def add_extra(self, key, value):
        if self.extra is None:
            self.extra = {}
        self.extra[key] = value

    @staticmethod
    def empty(stage_name: Union[str, None]) -> "ConnectorMetrics":
        metrics = ConnectorMetrics(stage_name=stage_name, images_total=0, time_total_s=0)
        return metrics

    def as_dict(self, recursive=False):
        me_as_dict = {
            'stage_name': self.stage_name,
            'time_total_s': self.time_total_s,
        }
        if self.images_total is not None and self.images_total != 0:
            me_as_dict['images_total'] = self.images_total
            me_as_dict['seconds_per_image'] = round(self.time_total_s / self.images_total, 4)

        if self.extra is not None and len(self.extra) > 0:
            me_as_dict['extra'] = self.extra

        if recursive and self.stages is not None and len(self.stages) > 0:
            me_as_dict['stages'] = []
            for stage in self.stages:
                me_as_dict['stages'].append(stage.as_dict(recursive=True))

        encoded_dict = self.__encode_dict(me_as_dict)
        return encoded_dict

    def __str__(self):
        return json.dumps(self.as_dict(recursive=False), indent=4)

    def __repr__(self):
        return self.__str__()

    def pretty_print(self):
        return json.dumps(self.as_dict(recursive=True), indent=4)

    def __encode_dict(self, arg: Union[Dict, List]) -> Union[Dict, List]:
        if isinstance(arg, dict):
            for k in arg:
                v = arg[k]
                if isinstance(v, datetime.datetime):
                    arg[k] = v.isoformat()
                elif isinstance(v, uuid.UUID):
                    arg[k] = str(v)
                elif isinstance(v, dict) or isinstance(v, list):
                    arg[k] = self.__encode_dict(v)
        elif isinstance(arg, list):
            arg = [self.__encode_dict(x) for x in arg]
        return arg
