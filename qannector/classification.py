import logging
import math
from concurrent.futures import ThreadPoolExecutor
from typing import List, Tuple, Optional, Callable

import multiprocess.pool
import numpy as np

from qannector import tensorflow_layer as tfl
from qannector.functions import divide_collection_into_batches
from qannector.image_manipulation import image_to_tensor
from qannector.kmp_duplicate_lib_decorator import save_and_restore_kmp_duplicate_lib_ok
from qannector.metrics import log_metrics, ConnectorMetrics
from qannector.model_urls import get_model_predict_url
from qannector.parallelism_mode import ParallelismMode


def __validate_and_set_batch_size(base_batch_size: int,
                                  thread_count: int,
                                  image_count: int,
                                  comfort_max_batch_size: Optional[int],
                                  logger: logging.Logger) -> int:
    if comfort_max_batch_size is None or comfort_max_batch_size <= 0:
        return base_batch_size

    if comfort_max_batch_size * thread_count >= image_count:
        batch_size = math.ceil(image_count / thread_count)
        logger.debug(f'Batch size set {batch_size} due to comforting algorythm. '
                     f'image_count: {image_count}, thread_count: {thread_count}, '
                     f'comfort_max_batch_size: {comfort_max_batch_size}')
        return batch_size

    return base_batch_size


@save_and_restore_kmp_duplicate_lib_ok
@log_metrics('qannector.metrics')
def apply_classification_model(images: List[np.ndarray],
                               tfs_url: str,
                               model_name: str,
                               model_version: int = 1,
                               batch_size: int = 90,
                               comfort_max_batch_size: Optional[int] = None,
                               thread_count: int = 4,
                               parallelism_mode: int = -1,
                               normalization: Optional[Callable[[np.ndarray], np.ndarray]] = None,
                               request_timeout_s: int = 300,
                               max_retry_count: int = 1,
                               strict_mode_logging: bool = False,
                               channels: Optional[List[int]] = None) -> Tuple[
    List[np.ndarray], ConnectorMetrics]:
    """
    Applies remote ANN classification model (served by TensorFlow Serving) to a list of images in a series of API
    requests, one request for each batch. The list of images is broken into multiple parts, each part processed in a
    separate thread. Images should be properly sized beforehand.

    :param strict_mode_logging: If set to False all non-critical warnings will be presented
    in logs in INFO levels instead of WARNING level
    :param max_retry_count: Max amount of retries to request TFS (in given timeout).
    :param request_timeout_s: Timeout in seconds for TFS prediction (in seconds). Default 300 seconds (5 min).
    :param parallelism_mode: parallelism mode. Use ParallelismMode.PARALLELISM_MODE_* constants to fill correctly.
    Default multiprocess.
    :param images: List of images (np.ndarray) to process
    :param model_name: ANN model name on TFS
    :param model_version: ANN model version
    :param tfs_url: TFS rest api endpoint URL
    :param batch_size: number of images smushed in a single query to ANN
    :param thread_count: number of parallel threads to run. If 0 or 1 passed single-threaded version of a function will
     be run.
    :param normalization: Normalization function to apply when image is converted into tensor
    :return: Results of ANN processing, list of probabilities of classes (np.ndarray)
    """

    if type(images) != list:
        raise ValueError(
            f'Parameter images expected to be a list of images (np.ndarray): List[np.ndarray], received {type(images)} instead')

    if images is None or len(images) == 0:
        return [], ConnectorMetrics.empty(stage_name='TFSClassification')

    _metrics = ConnectorMetrics.start(stage_name='TFSClassification', images_total=len(images))

    main_logger = logging.getLogger('qannector.main')

    validation_stage_metrics = ConnectorMetrics.start('Validation', images_total=len(images))

    parallelism_mode = ParallelismMode.validate_and_set_parallelism_mode(parallelism_mode, main_logger)
    thread_count = __validate_and_set_thread_count(thread_count, main_logger, strict_mode_logging)

    batch_size = __validate_and_set_batch_size(batch_size, thread_count, len(images),
                                               comfort_max_batch_size, main_logger)

    _metrics.register_stage(validation_stage_metrics.stop())

    model_predict_url = get_model_predict_url(tfs_url, model_name, model_version)
    images_split = divide_collection_into_batches(images, batch_size)
    main_logger.debug(f'Batch size: {batch_size}, '
                      f'total images: {len(images)},'
                      f' total batches count: {len(images_split)}, '
                      f'batches: {[len(b) for b in images_split]}')

    result = []

    if parallelism_mode == ParallelismMode.PARALLELISM_MODE_MULTIPROCESS:
        with multiprocess.pool.Pool(thread_count) as pool:
            mpp_results = [pool.apply_async(__apply_classification_model_to_batch,
                                            (
                                                images_slice,
                                                batch_number,
                                                model_predict_url,
                                                normalization,
                                                request_timeout_s,
                                                max_retry_count,
                                                channels)
                                            ) for (batch_number, images_slice) in enumerate(images_split)]

            for mpp_result in mpp_results:
                batch_results, stages_metrics = mpp_result.get()
                result.extend(batch_results)
                _metrics.register_stage(stages_metrics)

    elif parallelism_mode == ParallelismMode.PARALLELISM_MODE_THREADPOOL:
        with ThreadPoolExecutor(max_workers=thread_count) as executor:
            futures = [
                executor.submit(__apply_classification_model_to_batch,
                                images_slice,
                                batch_number,
                                model_predict_url,
                                normalization,
                                request_timeout_s,
                                max_retry_count,
                                channels)

                for (batch_number, images_slice) in enumerate(images_split)]

            for future in futures:
                batch_results, stages_metrics = future.result()
                result.extend(batch_results)
                _metrics.register_stage(stages_metrics)

    _metrics.stop()
    main_logger.debug(_metrics)

    return result, _metrics


def __validate_and_set_thread_count(thread_count, main_logger, strict_mode_logging):
    if thread_count == 0:
        main_logger.log(level=logging.WARNING if strict_mode_logging else logging.INFO,
                        msg='Parameter thread_count passed == 0. Did you mean 1? Running running as if thread_count was 1.')
        thread_count = 1
    return thread_count


@log_metrics('qannector.metrics')
def __apply_classification_model_to_batch(all_images: List[np.ndarray],
                                          batch_number: int,
                                          model_predict_url: str,
                                          normalization: Optional[Callable[[np.ndarray], np.ndarray]] = None,
                                          request_timeout_s: int = 300,
                                          max_retry_count: int = 1,
                                          channels: Optional[List[int]] = None) \
        -> Tuple[List[np.ndarray], ConnectorMetrics]:
    """
    Applies remote TensorFlow Serving classification model to a batch of images

    :param all_images: Full list of images (to avoid extra copying)
    :param model_predict_url: TensorFlow Serving REST API Predict method of a chosen model
    :param normalization: Normalization function to apply when image is converted into tensor
    :param max_retry_count: Max amount of retries to request TFS (in given timeout).
    :param request_timeout_s: Timeout in seconds for TFS prediction (in seconds). Default 300 seconds (5 min).
    :return: List of resulting probabilities of classes
    """
    _metrics = ConnectorMetrics.start(stage_name=f'ApplyClassificationModelToBatch#{batch_number}',
                                      images_total=len(all_images))
    create_tensor_batch_metrics = ConnectorMetrics.start(stage_name="CreateTensorBatch",
                                                         images_total=len(all_images))
    tensor_batch = [image_to_tensor(image, normalization) for image in all_images]
    _metrics.register_stage(create_tensor_batch_metrics)

    predictions, prediction_metrics = tfl.get_tensorflow_prediction(tensor_batch, model_predict_url,
                                                                    request_timeout_s=request_timeout_s,
                                                                    max_retry_count=max_retry_count)

    _metrics.register_stage(prediction_metrics)

    convert_predictions_to_float32_metrics = ConnectorMetrics.start(stage_name="ConvertPredictionsToFloat32",
                                                                    images_total=len(predictions))
    predictions_as_np = [np.asarray(prediction, dtype=np.float32) for prediction in predictions]

    _metrics.register_stage(convert_predictions_to_float32_metrics)

    if channels is not None and len(channels) > 0:
        channels_extracting_stage = ConnectorMetrics.start("ChannelExtraction", images_total=len(predictions_as_np))
        predictions_as_np = [item[..., channels] for item in predictions_as_np]
        _metrics.register_stage(channels_extracting_stage)


    return predictions_as_np, _metrics


def parse_classification_results_into_classes_list(classification_results: List[np.ndarray]) -> np.ndarray:
    """
    Collapses classification results into a list of classes
    :param classification_results: TensorFlow classification prediction results
    :return: a list of classes
    """
    result = np.argmax(classification_results, -1).astype(np.uint16)
    return result
