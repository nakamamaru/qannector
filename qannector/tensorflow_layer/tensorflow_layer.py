import datetime
from typing import List, Tuple

import numpy as np
import requests
from orjson import orjson

from qannector.metrics import log_metrics, ConnectorMetrics


@log_metrics('qannector.metrics')
def get_tensorflow_prediction(chunks_as_tensors: List[np.ndarray], predict_url: str, request_timeout_s: int = 300,
                              max_retry_count: int = 1) -> Tuple[list, ConnectorMetrics]:
    """
    Sends a linear list of tensors to the TensorFlow Serving REST API predict method
    :param max_retry_count: Max amount of retries to request TFS (in given timeout).
    :param request_timeout_s: Timeout in seconds for TFS prediction (in seconds). Default 300 seconds (5 min).
    :param chunks_as_tensors: A linear list of tensors to process
    :param predict_url: TensorFlow Serving REST API predict method Url
    :return: List of lists with TFS results
    """
    _metrics = ConnectorMetrics.start(f"GetTensorflowPrediction", len(chunks_as_tensors))

    _prepare_predict_request_json_stage_metrics = ConnectorMetrics.start("PreparePredictRequestJson",
                                                                         len(chunks_as_tensors))
    predict_request = __prepare_request_json(chunks_as_tensors)
    _metrics.register_stage(_prepare_predict_request_json_stage_metrics)

    _post_predict_request_stage_metrics = ConnectorMetrics.start("PostPredictJson",
                                                                 len(chunks_as_tensors))
    response = __post_predict_request(predict_url, predict_request, request_timeout_s, max_retry_count)
    _post_predict_request_stage_metrics.stop()
    _post_predict_request_stage_metrics.add_extra('predict_request_length_bytes', len(predict_request))
    _metrics.register_stage(_post_predict_request_stage_metrics)

    _parse_response_json_metrics = ConnectorMetrics.start("ParseResponseJson")
    prediction = __parse_response_json(response)

    _parse_response_json_metrics.stop()
    _parse_response_json_metrics.images_total = len(prediction)
    _metrics.register_stage(_parse_response_json_metrics)

    return prediction, _metrics.stop()


@log_metrics('qannector.metrics')
def __prepare_request_json(tensors: List[np.ndarray]) -> bytes:
    """
    Serializes a list of tensors into TFS API request. Made a separate method for logging and profiling purposes.
    """
    instances = [x.tolist() for x in tensors]
    predict_request = orjson.dumps({'instances': instances}, option=orjson.OPT_SERIALIZE_NUMPY)
    return predict_request


@log_metrics('qannector.metrics')
def __post_predict_request(tf_serving_url: str, predict_request: bytes, timeout_s: int,
                           max_retry_count: int) -> requests.Response:
    """
    Posts data to a Predict TFS API method. Made a separate method for logging and profiling purposes.
    """

    session_start = datetime.datetime.now()

    last_exception = None

    retry_count = 0
    while retry_count <= max_retry_count and (datetime.datetime.now() - session_start).total_seconds() < timeout_s:
        try:
            return requests.post(tf_serving_url, predict_request, timeout=timeout_s)
        except requests.Timeout or requests.ReadTimeout:
            raise
        except Exception as e:
            last_exception = e
            retry_count += 1

    if last_exception is not None:
        raise requests.ConnectionError(
            f'ConnectionError: Max retries ({max_retry_count}) exceeded with url for given '
            f'timeout. (Caused by: {last_exception})').with_traceback(last_exception.__traceback__)
    else:
        raise requests.ConnectionError(
            f'ConnectionError: Max retries ({max_retry_count}) exceeded with url for given timeout.')


def __unprocessible_enitity_response_error(response: requests.Response):
    message = f'Tensorflow API Service returned unprocessable response: HTTP-code: {response.status_code}'
    if response.content is not None:
        message += f' {response.content}'
    return Exception(message)


def __get_tensorflow_exception(response: requests.Response) -> Exception:
    if response.status_code == 504:
        return TimeoutError(f'Tensorflow API Service returned 504 Timeout response: {response.content}')
    return __unprocessible_enitity_response_error(response)


@log_metrics('qannector.metrics')
def __parse_response_json(response: requests.Response) -> list:
    """
    Deserializes json response containing predictions into list of pixels. Made a separate method for
    logging and profiling purposes.
    """

    if response.ok:
        _content = orjson.loads(response.content)
        if 'predictions' in _content:
            return _content['predictions']
        else:
            raise __unprocessible_enitity_response_error(response)
    else:
        exception = __get_tensorflow_exception(response)
        raise exception
